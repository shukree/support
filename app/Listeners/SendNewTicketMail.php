<?php

namespace App\Listeners;

use Mail;
use App\Events\TicketCreated;
use App\Mail\TicketCreated as NewTicketMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNewTicketMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\TicketCreated  $event
     * @return void
     */
    public function handle(TicketCreated $event)
    {
        if (isset($event->ticket->email)) {
            // send the new ticket notification to user
            Mail::to($event->ticket->email)->send(new NewTicketMail($event->ticket));
          }
    }
}
