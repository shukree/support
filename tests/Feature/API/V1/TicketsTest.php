<?php

namespace Tests\Feature\API\V1;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TicketsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
            $response = $this->withHeaders([
                'Accept' => 'application/json',
        ])->post('/api/v1/tickets', [
            'customer_name' => 'Gemmy Sintana',
            'email' => 'gemtana@gmail.com',
            'phone' => '+94855523',
            'description' => 'There is something in my CPU',
        ]);

            $response->assertJson([
                'data' => [
                    'email' => 'gemtana@gmail.com',
                ]
            ]);
            $response->assertStatus(200);
    }

    public function testView()
    {
        $response = $this->json('GET', '/api/v1/tickets');
        $response->assertStatus(200);

        $response->assertJsonStructure(
            [
                [
                    'id',
                    'customer_name',
                    'email',
                    'phone',
                    'description',
                    'ref',
                    'status',
                    'created_at',
                    'updated_at',
                    'comments'
                ]
                
            ]
        
        );
    }

    public function testSearchFail()
    {
        $response = $this->json('GET', '/api/v1/tickets/03598cd2718efbd31b6c0b04c857bb609f2be');

        $response->assertStatus(404);
    }

    public function testSearchPass()
    {
        $response = $this->json('GET', '/api/v1/tickets/03598cd2718efbd31b6c0b04c857bb609f2be150');

        $response->assertStatus(200);
    }
}
