@extends('layouts.app')

@section('content')


<div class="text-center mt-5">
    <h1>Support Ticket</h1>
    <div class="m-5">

        <div class="row justify-content-center">
            <div class="col-lg-6 text-left">

                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>Ticket Refererence</th>
                            <td>{{ $ticket->ref }}</td>
                        </tr>
                        <tr>
                            <th>Customer Name</th>
                            <td>{{ $ticket->customer_name }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $ticket->email }}</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>{{ $ticket->phone }}</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>{{ $ticket->description }}</td>
                        </tr>
                    </tbody>
                </table>
                
                @if($ticket->comments->isNotEmpty())
                <div class="comments" id="comments">
                    @foreach($ticket->comments as $comment)
                    <div class="comment mt-5">
                        <div class="comment-author text-muted small">
                            <strong>
                            @if (isset($comment->user->name))
                                {{ $comment->user->name }}
                            @else
                                {{ $ticket->customer_name }}
                            @endif
                            </strong>
                            at
                            {{ $comment->created_at->format('d M Y h:i a') }}
                        </div>
                        <div class="comment-content">
                            {{ $comment->content }}
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
                <hr>
                <div class="comments-container mt-3">
                    <div class="comments"></div>
                    <div class="comment-editor">
                        <form id="comment-form" class="" action="{{ route('comments.store') }}" method="post">
                            @csrf
                            <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">
                            <div class="form-group">
                                <label for="content">Reply to this ticket:</label>
                                <textarea name="content" rows="3" class="form-control" placeholder="Enter your message here"></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button type="submit" name="button" class="btn btn-success">
                                    <i class="fa fa-spinner fa-spin d-none"></i>
                                    Add Reply
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
        <a class="btn btn-info mt-4" href="/">Go Back</a>
    </div>
</div>

@section('script')
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#comment-form').on('submit', function(evt) {
            evt.preventDefault();
            $form = $(this);
            $form.find('button i.fa').removeClass('d-none');
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                success: function(data) {
                    let commentsHTML = $('<div>').append(data).find('#comments').html();
                    $('#comments').html(commentsHTML);
                    $form.find('[name="content"]').val('');
                },
                error: function(jqXHR) {
                    let data = JSON.parse(jqXHR.responseText);
                    if (data.message) {
                        alert(data.message);
                    } else {
                        alert('Oops! Something went wrong.');
                    }
                },
                complete: function() {
                    $form.find('button i.fa').addClass('d-none');
                }
            });
        })
    });
</script>
@endsection

@endsection