<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        @stack('styles')
    </head>
    <body class="antialiased">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">OSS</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-toggle-menu" aria-controls="navbar-toggle-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbar-toggle-menu">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('tickets.index') }}">Tickets</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#features">Fatures</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#how-it-works">How It Works</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        @if(auth()->check())
                        <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                        @else
                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>


        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif

        @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        
        @yield('content')



            <!-- Footer -->
    <footer class="text-lg-start bg-dark text-muted">
    
        <!-- Section: Links  -->
        <section class="">
        <div class="container text-md-start">
            <!-- Grid row -->
            <div class="row">
               
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                <!-- Links -->
                
                <p class="footer-text">
                <a href="#!" class="text-reset">About Us</a>
                </p>
                <p>
                <a href="#!" class="text-reset">Meet the Team</a>
                </p>
                <p>
                <a href="#!" class="text-reset">Awards</a>
                </p>
            </div>
            <!-- Grid column -->
    
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                <!-- Links -->
                
                <p class="footer-text">
                <a href="#!" class="text-reset">Documentation</a>
                </p>
                <p>
                <a href="#!" class="text-reset">Developers</a>
                </p>
                <p>
                <a href="#!" class="text-reset">Careers</a>
                </p>
            </div>
            <!-- Grid column -->
    
            <!-- Grid column -->
            <div class="col-md-8 col-lg-6 col-xl-3 mx-auto mb-md-0 mb-4">
                <!-- Links -->
                <h6 class="text-uppercase fw-bold mb-4 footer-text">
                Subscribe to Newsletter
                </h6>
                
                <form>
                    <div class="form-row">
                      <div class="col-md-8">
                        <input type="text" class="form-control" placeholder="Your Email">
                      </div>
                      <div class="col-md-4 ">
                        <button type="submit" class="btn btn-primary mb-2 newsbtn">Subscribe</button>
                      </div>
                    </div>
                  </form>
            </div>
            <!-- Grid column -->
            </div>
            <!-- Grid row -->
        </div>
        </section>
        <!-- Section: Links  -->
    
        <!-- Copyright -->
        <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
         Copyright © 2021 - Elegant Media
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
        <script
        src="https://code.jquery.com/jquery-2.2.4.js"
        integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

        @yield('script')
    </body>
</html>