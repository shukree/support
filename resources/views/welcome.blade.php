@extends('layouts.app')

@section('content')

<section>
    <div class="hero-bg">
        <div class="shading">
            <div class="container text-center text-md-left pt-5">
                <h1>Support System</h1>
                <div class="mt-4">
                    <a href="{{ route('tickets.create') }}" class="btn btn-lg btn-primary">Open New Ticket</a>
                </div>
                <div class="mt-5">
                    <p>
                        Check the status of your ticket:
                    </p>
                    <form class="" action="{{ route('tickets.search') }}" method="get">
                        <div class="row">
                            <div class="col-md-6 col-lg-4">
                                <input type="text" name="reference" value="" class="form-control mb-2" placeholder="Enter ticket reference">
                            </div>
                            <div class="col-md-4 col-lg-2">
                                <button type="submit" name="view" class="btn btn-success w-100">View Ticket</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="features">
    <div class="features-bg mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-3">
                    <h3>Fast Response</h3>
                    <p class="text-muted">Our dedicated support team is available 24/7 to help you with technical issues.</p>
                </div>
                <div class="col-md-4 mb-3">
                    <h3>No Login Required</h3>
                    <p class="text-muted">Get help with a single click. Track your support ticklet with a tracking code.</p>
                </div>
                <div class="col-md-4 mb-3">
                    <h3>Friendly Service</h3>
                    <p class="text-muted">Solve your issue with the support from a friend. We care your experience.</p>
                </div>
            </div>
        </div>
    </div>
</section>
    
@endsection

@push('styles')
<style media="screen">
    .hero-bg {
        background-image: url({{ asset('images/hero-bg.jpg') }});
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: scroll;
        background-position: top right;
        color: white;
    }

    .hero-bg .shading {
        background-color: #000000aa;
        padding: 50px 0;
        min-height: 500px;
    }
</style>
@endpush
        
